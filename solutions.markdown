---
title: Solutions
position: 11
lang: en
ref: solutions
excerpt: Moxa specialises by market and offers solutions for rail, ITS, smart grid,
  oil&gas, and marine.
layout: page
---

[MOXA solutions for rail](https://www.moxa.com/Solutions/Railway/Solution/Overview.htm){:target="_blank"}
![MOXA solutions for rail](/uploads/overview%20-%20railway.jpg){:target="_blank"}

[MOXA solutions for ITS](https://www.moxa.com/Solutions/ITS/index.htm){:target="_blank"}
![MOXA solutions for ITS](/uploads/overview_header.jpg){:target="_blank"}

[MOXA solutions for smart grid](https://www.moxa.com/Solutions/SmartGrid/index.htm){:target="_blank"}
![MOXA solutions for smart grid](/uploads/top6.jpg){:target="_blank"}

[MOXA solutions for oil&gas](https://www.moxa.com/Solutions/Oil_and_gas/index.htm){:target="_blank"}
![MOXA solutions for oil&gas](/uploads/solution_header_4.jpg){:target="_blank"}

[MOXA solutions for marine](https://www.moxa.com/solutions/maritime/web/index.htm){:target="_blank"}
![MOXA solutions for marine](/uploads/technology_header.jpg){:target="_blank"}

[Robustel solutions for cellular network](http://www.robustel.com/m2m-solutions/){:target="_blank"}
![Robustel solutions for cellular network](/uploads/robustel_m2m.jpg){:target="_blank"}

[C&T Solution for Fanless Embedded Computer Systems](http://www.candtsolution.com/en/goods_catalog.php?cid=1){:target="_blank"}

[![BCO-1030_Front-d52ec3.png](/uploads/BCO-1030_Front-d52ec3.png)](http://www.candtsolution.com/en/goods_catalog.php?cid=1){:target="_blank"}

[![BCO-1030_Rear-dbc2f2.png](/uploads/BCO-1030_Rear-dbc2f2.png)](http://www.candtsolution.com/en/goods_catalog.php?cid=1){:target="_blank"}

[C&T Solution for Industrial Display Systems](http://www.candtsolution.com/en/goods_catalog.php?cid=2){:target="_blank"}

[![VIO_Family_1-5c6258.png](/uploads/VIO_Family_1-5c6258.png)](http://www.candtsolution.com/en/goods_catalog.php?cid=2){:target="_blank"}

[C&T Solution for Industrial Computer Boards](http://www.candtsolution.com/en/goods_catalog.php?cid=3){:target="_blank"}

[![CT-MSL01-9dd366.png](/uploads/CT-MSL01-9dd366.png)](http://www.candtsolution.com/en/goods_catalog.php?cid=3){:target="_blank"}

