---
title: Partners
date: 2017-07-31 15:12:00 +03:00
position: 13
lang: en
ref: partners
excerpt: 'Juka LT is official Moxa distributor in Baltic countries. Juka LT is also
  partners with Robustel and BitStream. '
layout: page
---

### MOXA

![MOXA](/uploads/Moxa_Logo.jpg){:target="_blank"}

Moxa is a leading provider of edge connectivity, industrial computing, and network infrastructure solutions for enabling connectivity for the Industrial Internet of Things. With over 25 years of industry experience, Moxa has connected more than 40 million devices worldwide and has a distribution and service network that reaches customers in more than 70 countries. Moxa delivers lasting business value by empowering industry with reliable networks and sincere service for industrial communications infrastructures.

We distribute MOXA devices:

-**Switches**:

* Managed: EDS-400 series, EDS-500 series, EDS-500E, SDS-3008

* Unmanaged: EDS-200 series, EDS-300 series

* Mounted in 19” rack:  ICS-G7000 series, IKS-G6000 series, IKS-6000 series

-**Converters**: NPort, ioLogik, ICF, TCF, MGate, UPort and other models

-**Modems**: Oncell, IEX-400

<form action="http://juka.lt/en/contacts/"> <input type="submit" value="Submit quotation request" /> </form>

[Find out more about MOXA](https://www.moxa.com/about/Company_Profile.aspx){:target="_blank"}

[MOXA products and solutions](https://www.moxa.com/product/product.aspx){:target="_blank"}

### ROBUSTEL

![Robustel](/uploads/Robustel-Header.png){:target="_blank"}

Juka LT is official Robustel distributor in Baltic countries. Guangzhou Robustel Technologies Co., Ltd. is the leading industrial IoT/M2M Hardware and Solution Provider. Since its establishment in 2010 in Guangzhou China, Robustel has been concentrating on providing Industrial GoRugged Cellular Routers, Gateways, Modems, Cloud Platform and E2E Solutions. To support global business in more than 100 countries, Robustel has established branches in Germany, Australia, Japan, Netherlands and Hong Kong.

[Find out more about Robustel](http://www.robustel.com/about/){:target="_blank"}

[Robustel products](http://www.robustel.com/products/){:target="_blank"}

<form action="http://juka.lt/en/contacts/"> <input type="submit" value="Submit quotation request" /> </form>

### C&T Solution

![C&T_Logo_UnderlineText.jpg](/uploads/C&T_Logo_UnderlineText.jpg){:target="_blank"}

C&T Solution Inc. is JUKA LT partner that focuses on providing industrial/embedded computing solutions for industrial automation, medical computing, transportation, networking and other industries, C&T strives for the highest standards in innovation and technology, enabling the company to supply a wide ranging portfolio consisting of embedded computer systems, display systems, and industrial computer boards that are able to withstand extremes in environmental conditions, shock and vibration.

[Find out more about C&T Solution ](http://www.candtsolution.com/en/){:target="_blank"}

<form action="http://juka.lt/en/contacts/"> <input type="submit" value="Submit quotation request" /> </form>

### BitStream

![Bitstream](/uploads/bitstream_logo_niebieskie.png){:target="_blank"}

BitStream is Juka LT partner who is not only a manufacturer of equipment for data communication networks for wired and optical networks. BitStream is primarily a group of creative and experienced engineers, who saw a need for a niche market for the device and offered their own original solutions.

[Find out more about BitStream](http://bitstream.com.pl/en/Page/index/id/73){:target="_blank"}

[BitStream products](http://bitstream.com.pl/en/Page/index/id/74){:target="_blank"}

<form action="http://juka.lt/en/contacts/"> <input type="submit" value="Submit quotation request" /> </form>