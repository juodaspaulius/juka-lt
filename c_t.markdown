---
title: C&T Solution
date: 2018-08-09 06:46:00 +03:00
position: 5
lang: lt
ref: c_t
parent: partners
layout: page
---

![C&T_Logo_UnderlineText.jpg](/uploads/C&T_Logo_UnderlineText.jpg){:target="_blank"}

Juka LT atstovaujamas pramoninių kompiuterių gamintojas C&T Solutions inc. kuria sprendimus skirtus pramonės automatizavimui, transporto ir geležinkelių sektoriams.
C&T Solutions siūlo pramoninius kompiuterius, pramoninius monitorius ir įvairias kompiuterių plokštes, skirtus sudėtingoms aplinkos sąlygoms.

[Daugiau apie C&T Solution ](http://www.candtsolution.com/en/){:target="_blank"}

<form action="http://juka.lt/kontaktai/"> <input type="submit" value="Pateik prekės užklausimą" /> </form>
