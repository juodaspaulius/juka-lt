<?PHP

class iniParser {
    private static $parsedFile = null;
    public static function String($word) {
        if (self::$parsedFile === null) {
            self::$parsedFile = parse_ini_file("./.user.ini");
        }
        if (isset(self::$parsedFile[$word])) {
            return self::$parsedFile[$word];
        }
        return null;
    }
}


// form handler
if ($_POST && isset($_POST['name'], $_POST['email'], $_POST['message'])) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    $captcha=$_POST['g-recaptcha-response'];

    if (!$captcha){
        http_response_code(400);
        exit;
    }
    $catcha_secret = iniParser::String("captcha_secret");
    $captcha_response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$catcha_secret."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);

    if ($captcha_response['success'] == false){
        $errorMsg = "Spammers are not welcome";
    } elseif (!$name) {
        $errorMsg = "Please enter your Name";
    } elseif (!$email || !preg_match("/^\S+@\S+$/", $email)) {
        $errorMsg = "Please enter a valid Email address";
    } elseif (!$message) {
        $errorMsg = "Please enter your comment in the Message box";
    } else {
        // send email and redirect
        $to = "info@juka.lt";
        $subject = "Užklausa iš svetainės";
        $headers = "Content-Type: text/html; charset=UTF-8\r\nFrom: " . $email . "\r\n";
        mail($to, $subject, $message, $headers);
        http_response_code(200);
        exit;
    }
    if ($errorMsg){
        http_response_code(400);
    }
}
?>