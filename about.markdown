---
title: About
date: 2017-09-14 16:41:00 +03:00
position: 15
lang: en
ref: about
layout: page
---

**JUKA LT, UAB**  is industrial networking products and solutions distributor in Baltic countries. We have partnerships with **MOXA**, **ROBUSTEL** and **BitStream**. You can find out more about our partners [here](http://juka.lt/partners/){:target="_blank"}.

Our visions is to become a number one industrial networking value added distributor for SI in Baltics.

JUKA LT is making additional value to the SI and End user by:

* Offering technical support on products

* Making the current network audit

* Making the networking topology drawings with the offered products

* Setting up the network devices