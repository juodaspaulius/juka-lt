---
title: Contacts
position: 17
ref: contacts
layout: page
---

Juka LT, UAB

Registration number: 304009366

VAT number: LT100010937418

Address: Parodų g. 4, LT-04133 Vilnius

Mobile: \+37062913082

E-mail: [info@juka.lt](mailto:info@juka.lt)

{% include contact-form-en.html %}

You can find us here:
<iframe src="https://www.google.com/maps/place/Juka+LT/@54.673345,25.228067,16z/data=!4m13!1m7!3m6!1s0x46dd9384f16a8e77:0x695fb628fd2e3b84!2sParod%C5%B3+g.+4,+Vilnius+04132,+Lietuva!3b1!8m2!3d54.6733449!4d25.228067!3m4!1s0x46dd9384f16a8e77:0x7c564ad0130176b7!8m2!3d54.6733449!4d25.228067?hl=lt" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
