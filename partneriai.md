---
title: Partneriai
permalink: "/partneriai/"
position: 12
ref: partners
excerpt: 'Juka LT yra pramoninio tinklo įrenginių gamintojo MOXA oficialus atstovas
  Lietuvoje ir Baltijos šalyse. Taip pat Robustel ir BitStream partneriai. '
layout: page
---

### MOXA

![Moxa](/uploads/Moxa_Logo.jpg){:target="_blank"}

Juka LT yra pramoninio tinklo įrenginių gamintojo MOXA oficialus atstovas Lietuvoje ir Baltijos šalyse. Daugiau nei 20 metų pramoninių sistemų integratoriai pasitiki MOXA įrenginiais. Dirbdami su sertifikuotais integratoriais MOXA siūlo profesionalius pramoninius sprendimus. MOXA gaminiai atitinka ISO 9001 ir ISO 14001 sertifikatus. MOXA įkurta 1987 Taivanyje, šiuo metu pagrindinė buveinė yra JAV.

Prekiaujame MOXA įranga:

-**Komutatoriais**:

* Valdomi: EDS-400 serija, EDS-500 serija, EDS-500E, SDS-3008

* Nevaldomi: EDS-200 serija, EDS-300 serija

* Montuojami į 19” spintą:  ICS-G7000 serija, IKS-G6000 serija, IKS-6000 serija

-**Keitikliais**: NPort, ioLogik, ICF, TCF, MGate, UPort ir kitais modeliais

-**Modemais**: Oncell, IEX-400

<form action="http://juka.lt/kontaktai/"> <input type="submit" value="Pateik prekės užklausimą" /> </form>

[Daugiau apie MOXA](https://www.moxa.com/about/Company_Profile.aspx){:target="_blank"}

[MOXA produktai ir sprendimai](https://www.moxa.com/product/product.aspx){:target="_blank"}

### ROBUSTEL

![Robustel](/uploads/Robustel-Header.png){:target="_blank"}

Juka LT yra oficialus Robustel Technologies atstovas Lietuvoje. Robustel Technologies yra pirmaujanti pramonės IoT/M2M sprendimų tiekėja. Nuo savo įkūrimo 2010 m. Guangžou Kinijoje Robustel daugiausia dėmesio skyrė „Industrial GoRugged“ korinio ryšio maršrutizatorių, tinklų sąsajų, modemų, debesų platformų ir E2E sprendimų tiekimui. Robustel yra Kinijos įmonė, tačiau turi filialą Vokietijoje.

[Daugiau apie Robustel](http://www.robustel.com/about/){:target="_blank"}

[Robustel produktai](http://www.robustel.com/products/){:target="_blank"}

<form action="http://juka.lt/kontaktai/"> <input type="submit" value="Pateik prekės užklausimą" /> </form>

### C&T Solution

![C&T_Logo_UnderlineText.jpg](/uploads/C&T_Logo_UnderlineText.jpg){:target="_blank"}

Juka LT atstovaujamas pramoninių kompiuterių gamintojas C&T Solutions inc. kuria sprendimus skirtus pramonės automatizavimui, transporto ir geležinkelių sektoriams.
C&T Solutions siūlo pramoninius kompiuterius, pramoninius monitorius ir įvairias kompiuterių plokštes, skirtus sudėtingoms aplinkos sąlygoms.

[Daugiau apie C&T Solution ](http://www.candtsolution.com/en/){:target="_blank"}

<form action="http://juka.lt/kontaktai/"> <input type="submit" value="Pateik prekės užklausimą" /> </form>

### BitStream

![Bitstream](/uploads/bitstream_logo_niebieskie.png){:target="_blank"}

BitSream yra Juka, LT partneriai, kurie yra ne tik laidinių bei optinių duomenų perdavimo tinklų įrangos gamintojai - Bistream visų pirma yra kūrybingų ir patyrusių inžinierių komanda, kurie įžvelgę rinkoje nišą bei paklausą pasiūlė savo originalius sprendimus.

[Daugiau apie BitStream ](http://bitstream.com.pl/en/Page/index/id/73){:target="_blank"}

[Bitstream produktai](http://bitstream.com.pl/en/Page/index/id/74){:target="_blank"}

<form action="http://juka.lt/kontaktai/"> <input type="submit" value="Pateik prekės užklausimą" /> </form>