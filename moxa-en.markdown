---
title: Moxa
date: 2018-08-09 06:46:00 +03:00
position: 0
lang: en
ref: moxa
parent: partners
layout: page
---

![MOXA](/uploads/Moxa_Logo.jpg){:target="_blank"}

Moxa is a leading provider of edge connectivity, industrial computing, and network infrastructure solutions for enabling connectivity for the Industrial Internet of Things. With over 25 years of industry experience, Moxa has connected more than 40 million devices worldwide and has a distribution and service network that reaches customers in more than 70 countries. Moxa delivers lasting business value by empowering industry with reliable networks and sincere service for industrial communications infrastructures.

We distribute MOXA devices:

-**Switches**:

* Managed: EDS-400 series, EDS-500 series, EDS-500E, SDS-3008

* Unmanaged: EDS-200 series, EDS-300 series

* Mounted in 19” rack:  ICS-G7000 series, IKS-G6000 series, IKS-6000 series

-**Converters**: NPort, ioLogik, ICF, TCF, MGate, UPort and other models

-**Modems**: Oncell, IEX-400

<form action="http://juka.lt/en/contacts/"> <input type="submit" value="Submit quotation request" /> </form>

[Find out more about MOXA](https://www.moxa.com/about/Company_Profile.aspx){:target="_blank"}

[MOXA products and solutions](https://www.moxa.com/product/product.aspx){:target="_blank"}
