---
title: Moxa
date: 2018-08-09 06:43:00 +03:00
position: 1
lang: lt
ref: moxa
parent: partners
layout: page
---

![Moxa](/uploads/Moxa_Logo.jpg){:target="_blank"}

Juka LT yra pramoninio tinklo įrenginių gamintojo MOXA oficialus atstovas Lietuvoje ir Baltijos šalyse. Daugiau nei 20 metų pramoninių sistemų integratoriai pasitiki MOXA įrenginiais. Dirbdami su sertifikuotais integratoriais MOXA siūlo profesionalius pramoninius sprendimus. MOXA gaminiai atitinka ISO 9001 ir ISO 14001 sertifikatus. MOXA įkurta 1987 Taivanyje, šiuo metu pagrindinė buveinė yra JAV.

Prekiaujame MOXA įranga:

-**Komutatoriais**:

* Valdomi: EDS-400 serija, EDS-500 serija, EDS-500E, SDS-3008

* Nevaldomi: EDS-200 serija, EDS-300 serija

* Montuojami į 19” spintą:  ICS-G7000 serija, IKS-G6000 serija, IKS-6000 serija

-**Keitikliais**: NPort, ioLogik, ICF, TCF, MGate, UPort ir kitais modeliais

-**Modemais**: Oncell, IEX-400

<form action="http://juka.lt/kontaktai/"> <input type="submit" value="Pateik prekės užklausimą" /> </form>

[Daugiau apie MOXA](https://www.moxa.com/about/Company_Profile.aspx){:target="_blank"}

[MOXA produktai ir sprendimai](https://www.moxa.com/product/product.aspx){:target="_blank"}

