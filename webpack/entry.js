import "babel-polyfill";

import ReactDOM from 'react-dom';
import React from 'react';

import Navigation from './nav/Navigation';

function gtmEvent(eventName) {
    if (window.dataLayer) {
        // google tag manager custom event
        window.dataLayer.push({
            event: eventName
        });
    }
}

$(() => {
    const form = $('#contact-us');
    if (form.length > 0) {
        form.submit((e) => {
            e.preventDefault();
            const data = form.serialize();
            $.post(form.attr('action'), data).then(() => {
                console.log('Sent!');
                form.addClass('sent');
                $('.success-message').show();
                $('.error-message').hide();
                gtmEvent('contactFormSuccess');
            }, () => {
                $('.error-message').show();
                $('.success-message').hide();
                gtmEvent('contactFormError');
            });
        });
    }
});

const navItems = window.NAV_MENU.map(page=>({...page , children: [] }))
const navHashmap = navItems.reduce((accum, page) => ({ ...accum, ...{ [page.ref]: page}}), {});

navItems.forEach((page) => {
    if (page.parent && navHashmap[page.parent]) {
        navHashmap[page.parent].children.push(page);
    }
});

const navConfig = Object.values(navHashmap).filter((page) => !page.parent);

ReactDOM.render(<Navigation navConfig={navConfig} />, document.getElementsByTagName('nav')[0]);
