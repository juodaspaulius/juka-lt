import React from 'react';

import Nav from 'react-bootstrap/lib/Nav';
import NavDropdown from 'react-bootstrap/lib/NavDropdown';
import NavItem from 'react-bootstrap/lib/NavItem';
import MenuItem from 'react-bootstrap/lib/MenuItem';

export default function Navigation(props) {
    return (
        <Nav>
            {
                props.navConfig.map(navItem =>
                    navItem.children && navItem.children.length > 0 ? (
                        <NavDropdown key={navItem.label + navItem.url} title={navItem.label}>
                            {navItem.children.map(child =>
                                <MenuItem key={child.label + child.url} href={child.url}>{child.label}</MenuItem>)}
                        </NavDropdown>) : (
                        navItem.label ?
                            <NavItem key={navItem.label + navItem.url}
                                     href={navItem.url}>{navItem.label}</NavItem> : null))
            }
        </Nav>
    );
}