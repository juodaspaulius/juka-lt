---
title: Kontaktai
permalink: "/kontaktai/"
position: 16
ref: contacts
layout: page
---

Juka LT, UAB

Įmonės kodas: 304009366

PVM mokėtojo kodas: LT100010937418

Adresas: Parodų g. 4, LT-04133 Vilnius

Mobilus telefonas: \+37062913082

El. paštas: [info@juka.lt](mailto:info@juka.lt)

{% include contact-form.html %}

Mus rasite:
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2307.061553772154!2d25.22587831619995!3d54.673344880278556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd9384f16a8e77%3A0x695fb628fd2e3b84!2sParod%C5%B3\+g.\+4%2C\+Vilnius\+04132!5e0!3m2!1sen!2slt!4v1500407404628" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>