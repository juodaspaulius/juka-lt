---
title: Robustel
date: 2018-08-09 06:46:00 +03:00
position: 2
lang: en
ref: robustel
parent: partners
layout: page
---

![Robustel](/uploads/Robustel-Header.png){:target="_blank"}

Juka LT is official Robustel distributor in Baltic countries. Guangzhou Robustel Technologies Co., Ltd. is the leading industrial IoT/M2M Hardware and Solution Provider. Since its establishment in 2010 in Guangzhou China, Robustel has been concentrating on providing Industrial GoRugged Cellular Routers, Gateways, Modems, Cloud Platform and E2E Solutions. To support global business in more than 100 countries, Robustel has established branches in Germany, Australia, Japan, Netherlands and Hong Kong.

[Find out more about Robustel](http://www.robustel.com/about/){:target="_blank"}

[Robustel products](http://www.robustel.com/products/){:target="_blank"}

<form action="http://juka.lt/en/contacts/"> <input type="submit" value="Submit quotation request" /> </form>
