---
title: Sprendimai
date: 2017-07-18 23:16:00 +03:00
position: 10
ref: solutions
excerpt: 'Moxa siūlo savo inovatyvią įrangą bei sprendimus geležinkeliams, keliams,
  energetikai, naftos pramonei bei laivybai. '
layout: page
---

[MOXA sprendimai geležinkeliams](https://www.moxa.com/Solutions/Railway/Solution/Overview.htm){:target="_blank"}
[![overview - railway.jpg](/uploads/overview%20-%20railway.jpg)](https://www.moxa.com/Solutions/Railway/Solution/Overview.htm){:target="_blank"}

[MOXA sprendimai keliams](https://www.moxa.com/Solutions/ITS/index.htm){:target="_blank"}
[![overview_header.jpg](/uploads/overview_header.jpg)](https://www.moxa.com/Solutions/ITS/index.htm){:target="_blank"}

[MOXA sprendimai energetikai](https://www.moxa.com/Solutions/SmartGrid/index.htm){:target="_blank"}
[![top6.jpg](/uploads/top6.jpg)](https://www.moxa.com/Solutions/SmartGrid/index.htm){:target="_blank"}

[MOXA sprendimai naftos pramonei](https://www.moxa.com/Solutions/Oil_and_gas/index.htm){:target="_blank"}
[![solution_header_4.jpg](/uploads/solution_header_4.jpg)](https://www.moxa.com/Solutions/Oil_and_gas/index.htm){:target="_blank"}

[MOXA sprendimai laivybai](https://www.moxa.com/solutions/maritime/web/index.htm){:target="_blank"}
[![technology_header.jpg](/uploads/technology_header.jpg)](https://www.moxa.com/solutions/maritime/web/index.htm){:target="_blank"}

[Robustel pramoniniai duomenų perdavimo GSM tinklu sprendimai](http://www.robustel.com/m2m-solutions/){:target="_blank"}
[![robustel_m2m.jpg](/uploads/robustel_m2m.jpg)](http://www.robustel.com/m2m-solutions/){:target="_blank"}

[C&T Sprendimai pramoniniams kompiuteriams](http://www.candtsolution.com/en/goods_catalog.php?cid=1){:target="_blank"}

[![BCO-1030_Front-d52ec3.png](/uploads/BCO-1030_Front-d52ec3.png)](http://www.candtsolution.com/en/goods_catalog.php?cid=1){:target="_blank"}

[![BCO-1030_Rear-dbc2f2.png](/uploads/BCO-1030_Rear-dbc2f2.png)](http://www.candtsolution.com/en/goods_catalog.php?cid=1){:target="_blank"}

[C&T Sprendimai pramoniniams monitoriams](http://www.candtsolution.com/en/goods_catalog.php?cid=2){:target="_blank"}

[![VIO_Family_1-5c6258.png](/uploads/VIO_Family_1-5c6258.png)](http://www.candtsolution.com/en/goods_catalog.php?cid=2){:target="_blank"}

[C&T Sprendimai pramoninių kompiuterių plokštėms](http://www.candtsolution.com/en/goods_catalog.php?cid=3){:target="_blank"}

[![CT-MSL01-9dd366.png](/uploads/CT-MSL01-9dd366.png)](http://www.candtsolution.com/en/goods_catalog.php?cid=3){:target="_blank"}