---
title: Robustel
date: 2018-08-09 06:46:00 +03:00
position: 3
lang: lt
ref: robustel
parent: partners
layout: page
---

![Robustel](/uploads/Robustel-Header.png){:target="_blank"}

Juka LT yra oficialus Robustel Technologies atstovas Lietuvoje. Robustel Technologies yra pirmaujanti pramonės IoT/M2M sprendimų tiekėja. Nuo savo įkūrimo 2010 m. Guangžou Kinijoje Robustel daugiausia dėmesio skyrė „Industrial GoRugged“ korinio ryšio maršrutizatorių, tinklų sąsajų, modemų, debesų platformų ir E2E sprendimų tiekimui. Robustel yra Kinijos įmonė, tačiau turi filialą Vokietijoje.

[Daugiau apie Robustel](http://www.robustel.com/about/){:target="_blank"}

[Robustel produktai](http://www.robustel.com/products/){:target="_blank"}

<form action="http://juka.lt/kontaktai/"> <input type="submit" value="Pateik prekės užklausimą" /> </form>
