---
title: Robustel M1200 is a Dual-SIM gateway
date: 2018-06-08 10:42:00 +03:00
lang: en
ref: m1200
layout: page
---

Robustel M1200 is a Dual-SIM gateway that enables remote data transmission to local serial ports and I/O. The M1200 supports a number of interfaces including RS-232, RS-485 and mini-USB. Providing users with stable network connectivity and data transmission.



See Product video [here](https://www.youtube.com/watch?v=esvkh3vhRrM&utm_source=newsletter&utm_medium=email&utm_campaign=reliable_and_cost_effective_micro_building_management_system_robustel_newsletter&utm_term=2018-06-07)