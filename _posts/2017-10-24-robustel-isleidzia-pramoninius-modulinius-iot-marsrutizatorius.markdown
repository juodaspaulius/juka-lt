---
title: Robustel išleidžia pramoninius modulinius IoT maršrutizatorius
date: 2017-10-24 10:41:00 +03:00
ref: robustel news
layout: page
---

![Robustel_to_Launch_Modular_Industrial_IoT_Edge_Gateway](/uploads/Robustel_to_Launch_Modular_Industrial_IoT_Edge_Gateway_at_SPS_IPC_Drives_2017_-_Press_Release_-_Robustel_Industrial_Wireless_M2M_-_2017-10-24_10.18.02.png)

Robustel pirmaujantis pramoninių IoT/M2M sprendimų tiekėjas planuoja išleisti naują pramoninį modulinį maršrutizatorių MEG5000. 

Skaitykite daugiau anglų kalba [čia.](http://juka.lt/robustel-to-launch-modular-industrial-iot-edge-gateway-at-sps-ipc-drives-2017/)
 