---
title: We are now official C&T Solutions distributor
date: 2018-01-16 16:43:00 +02:00
categories:
- news
tags:
- news
- distribution agreement
- new partners
lang: en
ref: cnt
layout: page
---

![VIO_Family_1-5c6258.png](/uploads/VIO_Family_1-5c6258.png)

**Juka LT** has signed the distribution agreement with **C&T Solutions Inc.** professional manufacturer that focus on providing industrial/embedded computing solutions for industrial automation, medical computing, transportation, networking and other industries. **C&T** strives for the highest standards in innovation and technology, enabling the company to supply a wide ranging portfolio consisting of embedded computer systems, display systems, and industrial computer boards that are able to withstand extremes in environmental conditions, shock and vibration.

Find more information about **C&T Solutions** [here. ](http://www.candtsolution.com/en/company.php?id=1){:target="_blank"}

<form action="http://juka.lt/en/contacts/"> <input type="submit" value="Submit quotation request" /> </form>

![C&T_Logo_UnderlineText.jpg](/uploads/C&T_Logo_UnderlineText.jpg)![BCO-1030_Rear-dbc2f2.png](/uploads/BCO-1030_Rear-dbc2f2.png)![BCO-1030_Front-d52ec3.png](/uploads/BCO-1030_Front-d52ec3.png)![CT-MSL01-9dd366.png](/uploads/CT-MSL01-9dd366.png)