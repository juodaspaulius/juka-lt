---
title: SDS-3008 puikiai tinkantis montuoti į automatikos spintas
date: 2018-01-18 15:36:00 +02:00
categories:
- moxa
- news
- industrial networking
tags:
- sds-3008
ref: sds3008
layout: page
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/oOQNWXkl9qw?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
 
Daugiau informacijos anglų kalba rasite [čia. ](https://www.moxa.com/Event/industrial-ethernet/smart-switch/index.htm){:target="_blank"}

[![snd-3008.JPG](/uploads/snd-3008.JPG)](https://www.moxa.com/Event/industrial-ethernet/smart-switch/index.htm){:target="_blank"}