---
title: Essential Solution in IoT Infrastructure - BCO-1000 series basic fanless embedded
  system
date: 2018-07-04 10:57:00 +03:00
lang: en
ref: bco-1000
layout: page
---

[![cnt.jpg](/uploads/cnt.jpg)](http://www.candtsolution.com/en/Promo/2018/BCO_1000/){:target="_blank"}

BCO-1000 series is the essential rugged computer series that carry on C&T’s modularized expansion design approach by providing maximum I/O features, while keeping a minimal footprint. With up to 3 expansion slots on any select model, you could configure up to 5 of SKUs to meet your specific requirements. C&T engineers specially selected non-moving parts which have contributed to the stability of BCO-1000 series in environments under constant shock and vibration. With a fanless design, BCO-1000 series can operate within a wide temperature range between -20°C to 50°C making it an ideal candidate for the harsh outdoor computing application without sacrificing space.

Key Industrial Features
* Supports Intel® Celeron® J1900 (2.0GHz) Quad Core Processor
* Fanless & cable-less design for maximum reliability
* Wide operating temperature range (-20°C to 50°C)
* Over-voltage protection
* Flexible expansion slots (mini PCIe)
* 5 Grms vibration tested
* 50G, 11ms shock tested

We’re proud to offer this superb product as part of our rugged computing line up. More information [here](http://www.candtsolution.com/en/Promo/2018/BCO_1000/){:target="_blank"}