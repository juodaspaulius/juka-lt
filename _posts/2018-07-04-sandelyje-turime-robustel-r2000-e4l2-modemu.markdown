---
title: Sandėlyje turime Robustel R2000-E4L2 modemų
date: 2018-07-04 12:16:00 +03:00
ref: R2000-E4L2
layout: page
---

[![R2000 Ent -1.jpg](/uploads/R2000%20Ent%20-1.jpg)](http://www.robustel.com/products/gorugged-industrial-cellular-r/r2000-ent-router.html){:target="_blank"}

Sandėlyje turime R2000-E4L2 modemų už patrauklią kainą - 330 EUR. Norėdami įsigyti rašykite info@juka.lt

Daugiau informacijos apie modemą [čia](http://www.robustel.com/products/gorugged-industrial-cellular-r/r2000-ent-router.html){:target="_blank"}