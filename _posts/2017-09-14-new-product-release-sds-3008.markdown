---
title: New product release - SDS 3008
date: 2017-09-14 14:50:00 +03:00
categories:
- industrial networking
- news
tags:
- ethernet switches
- moxa
- sds 3008
- industrial networking
- juka lt
lang: en
ref: news1
layout: page
---

![Capture - quick card.PNG](/uploads/Capture%20-%20quick%20card.PNG)

**Moxa** is introdusing the new industrial networking switches series SDS. The first product is Smart Switches SDS-3008 that are slim, simple and viewable on SCADAs.

The SDS-3008 smart Ethernet switch is the ideal product for IA engineers and automation machine builders to make their networks compatible with the vision of Industry 4.0.

**Features and Benefits**

* Compact and flexible housing design to fit into confined spaces

* Web-based graphical user interface design for easy device configuration and management

* EtherNet/IP, PROFINET, and Modbus/TCP industrial protocols supported for easy integration and monitoring in automation HMI/SCADA systems

* Supports RSTP/STP for network redundancy

* Security features based on IEC-62443

We currently have SDS-3008 Ethernet Switches at our warehouse if you are interested.

Find out more [here.](https://www.moxa.com/product/SDS-3008.htm){:target="_blank"}

![Capture - quik card 2.PNG](/uploads/Capture%20-%20quik%20card%202.PNG)