---
title: Robustel prezentacija IoT tema
date: 2018-04-03 15:16:00 +03:00
ref: sarun
layout: page
---

Robustel prezentacija IoT tema buvo pristatyta "Embedded World 2018" parodos metu.![robustel sarun.jpg](/uploads/robustel%20sarun.jpg)

Peržiūrėti prezentaciją galite [čia](http://events.techcast.com/embedded-world/archiv/EmbeddedWorld_Halle4_Mittwoch_0930/?utm_source=newsletter&utm_medium=email&utm_campaign=remote_access_to_siemens_plc_via_robustvpn_robustel_newsletter&utm_term=2018-04-02)