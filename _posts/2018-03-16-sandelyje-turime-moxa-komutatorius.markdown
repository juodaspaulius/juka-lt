---
title: Sandėlyje turime Moxa komutatorius
date: 2018-03-16 11:48:00 +02:00
ref: eds
layout: page
---

[![P1-promo-EDS205_208.jpg](/uploads/P1-promo-EDS205_208.jpg)](http://juka.lt/kontaktai/)

Perkant 10 ar daugiau **EDS-205** vienetų siūlome specialią kainą - 57€/vnt.

Perkant 10 ar daugiau **EDS-208** vienetų siūlome - 83€/vnt. 

Užsisakykite el-paštu info@juka.lt arba 
<form action="http://juka.lt/kontaktai/"> <input type="submit" value="Pateikite prekės užklausimą čia" /> </form>