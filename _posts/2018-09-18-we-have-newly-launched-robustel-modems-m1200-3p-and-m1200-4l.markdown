---
title: We have recently launched Robustel R1200 Gateways in stock
date: 2018-09-18 14:52:00 +03:00
lang: en
ref: m1200
layout: page
---

![M1200_Gateway_-_Cellular_Modems_Robustel.png](/uploads/M1200_Gateway_-_Cellular_Modems_Robustel.png)

Robustel M1200 is a Dual-SIM gateway that enables remote data transmission to local serial ports and I/O. The M1200 supports a number of interfaces including RS-232, RS-485 and mini-USB. Providing users with stable network connectivity and data transmission.

Ideal for Energy, Transportation and Agricultural applications.

<iframe width="560" height="315" src="https://www.youtube.com/embed/esvkh3vhRrM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

**Key Features**

* Auto GSM/GPRS/EDGE/UMTS/WCDMA/HSDPA/HSUPA/HSPA\+/FDD LTE/TDD LTE connections (No AT command required)

* Dual-SIM backup (push-push or MFF type optional)

* IPsec/OpenVPN/GRE/L2TP/PPTP

* Robust industrial design (9 to 36V DC, -40 to \+75 °C extended operating temperature, desktop or wall mounting or DIN rail mounting)

For more information go [here](http://www.robustel.com/products/gorugged-industrial-cellular-m/m1200-gateway.html){:target="_blank"}

To order please email us at info@juka.lt or 
<form action="http://juka.lt/en/contacts/"> <input type="submit" value="Submit request here" /></form>
