---
title: Robustel dalyvaus Hannover Messe 2018
date: 2018-04-03 14:59:00 +03:00
ref: messe
layout: page
---

Hannover Messe 2018 paroda vyks Balandžio 23-27 dienomis, kur Robustel kartu su partneriu Soracom pristatys naujausius IoT sprendimus, 8 salėje, C28 stende. 
![robustel naujiena.jpg](/uploads/robustel%20naujiena.jpg)

