---
title: Susitinkame Instrutec 2017 parodoje, Taline
date: 2017-11-03 12:40:00 +02:00
categories:
- news
tags:
- instrutec
- fair
- exhibition
ref: instrutec
layout: page
---

Lapkričio 15-17 dienomis dalyvausime Pramoninėje parodoje Instrutec 2017, Taline, Estijoje. 

![instrutec-2017.jpg](/uploads/instrutec-2017.jpg)

Mus gali sutikti **C-42 stende**. Mielai pabendrausime apie mūsų oficialius partnerius Moxa ir Robustel bei jų produktus ir paslaugas. 

Daugiau informacijos apie parodą [čia.](https://instrutec.ee/en/){:target="_blank"}

Parodos vieta: Pirita tee 28, Kesklinn, Tallinn, 10127 Harju Maakond, Eesti
