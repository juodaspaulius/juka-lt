---
title: Robustel R2000 video
date: 2019-01-02 14:22:00 +02:00
ref: r2000
layout: page
---

Robustel R2000 yra programuojamas VPN maršrutizatorius. Teikia patikimą ir nepertraukiamą ryšį M2M ir IoT programoms. Plačiai naudojamas bankomatuose, pardavimo automatuose, vaizdo stebėjimo sistemose ir kituose sprendimuose.
<iframe width="560" height="315" src="https://www.youtube.com/embed/hbzgftuu-js" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>