---
title: Learn more about Robustel R2000 router
date: 2019-01-02 13:53:00 +02:00
lang: en
ref: r2000
layout: page
---

The Robustel R2000 is a rugged, programmable, highly efficient VPN router. Providing reliable, seamless connectivity for M2M & IoT applications. Widely used in POS Terminals, ATM, Vending Machine, and CCTV. 
<iframe width="560" height="315" src="https://www.youtube.com/embed/hbzgftuu-js" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

