---
title: Meet us at Industrial fair Techindustry 2017 in Riga
date: 2017-11-27 13:03:00 +02:00
categories:
- news
tags:
- moxa
- robustel
- techindustry
- exhibition
- fair
lang: en
ref: techindustry
layout: page
---

![24169710_10155934972534173_1638739946_o.jpg](/uploads/24169710_10155934972534173_1638739946_o.jpg)

We are participating at Techinudustry 2017 fair in Riga, Latvia during 30th November – 2nd December.
 
Meet us at F-2 booth. We are looking forward to discuss with you about our official partners Moxa and Robustel and their solutions.

More information about the fair [here](http://www.techindustry.lv/en/index.php){:target="_blank"}. 

Fair location: Kipsala International exhibition centre (Kipsalas iela 8, Riga)

