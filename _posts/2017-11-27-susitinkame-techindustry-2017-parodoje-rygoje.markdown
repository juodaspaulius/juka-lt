---
title: Susitinkame Techindustry 2017 parodoje, Rygoje
date: 2017-11-27 12:56:00 +02:00
categories:
- news
tags:
- fair
- exhibition
- moxa
- robustel
- industrial networking
ref: techindustry
layout: page
---

![24169710_10155934972534173_1638739946_o-cfcf35.jpg](/uploads/24169710_10155934972534173_1638739946_o-cfcf35.jpg)

Lapkričio 30 – gruodžio 2 dienomis dalyvausime Pramoninėje parodoje Techindustry 2017, Rygoje, Latvijoje.

Mus gali sutikti F-2 stende. Mielai pabendrausime apie mūsų oficialius partnerius Moxa ir Robustel bei jų produktus ir paslaugas.

Daugiau informacijos apie parodą [čia](http://www.techindustry.lv/en/index.php){:target="_blank"}.

Parodos vieta: Kipsala International exhibition centre (Kipsalas iela 8, Riga)

Iki greito susitikimo!
