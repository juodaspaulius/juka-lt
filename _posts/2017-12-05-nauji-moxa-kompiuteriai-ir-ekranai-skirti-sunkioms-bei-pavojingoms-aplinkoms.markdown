---
title: Nauji MOXA kompiuteriai ir ekranai skirti sunkioms bei pavojingoms aplinkoms
date: 2017-12-05 11:51:00 +02:00
ref: computers
layout: page
---

![display.JPG](/uploads/display.JPG)

Veikimas pavojingoje aplinkoje sukelia daugybę kritinių problemų bei iššūkių, tarp kurių yra ypač šaltos ar karštos temperatūros pokyčiai, didelės dulkių ir vandens koncentracijos bei darbas sprogioje aplinkoje. 

MOXA siūlo patikimus kompiuterius ir ekranus, kurie idealiai tinka naudoti ekstremaliose aplinkose. 

Daugiau informacijos anglų kalba [čia](http://juka.lt/new-moxa-panel-computers-and-displays-for-tough-environments/){:target="_blank"}.

