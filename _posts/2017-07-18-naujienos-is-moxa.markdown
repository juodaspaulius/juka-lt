---
title: Naujienos iš MOXA
date: 2017-07-18 23:23:00 +03:00
categories:
- industrial networking
- moxa
tags:
- moxa
- news
- industrial networking
- pramoniniai tinklai
layout: post
---

MOXA sprendimai apsaugoti pramoninius tinklus
![edge to cloud.jpg](/uploads/edge%20to%20cloud.jpg){:target="_blank"}

[Skaityti daugiau](https://www.moxa.com/Event/integrated-solutions/security/edge-to-cloud/index.htm?utm_source=2017_07_Spotlight&utm_medium=email&utm_campaign=Spotlight&utm_content=Spotlight_Banner1&mkt_tok=eyJpIjoiTkRsa05EYzVaRE14WTJVNSIsInQiOiJxMVlmcXBKMnNkOFpnbUxHTVNGNWk1cjlTVTBKcUVMRGU3U2d4WGpIazRqNjd6UXpmOFRWWm9pUm0xTnoxV1VvNmYrbFNLdXNLbWx4N2x6eFgwNVdZKzQyRUZYK3BmR2F0cjlMd01NSGorRkRPVXpXV094RXVpalNHNVExMEVZbCJ9){:target="_blank"}

Norime pabrėžti, jog netinkamas atliekų tvarkymas daro žalą visuomenės sveikatai ir aplinkai. LR teisės aktai reikalauja pakuočių ir gaminių atliekas tinkamai rūšiuoti ir nešalinti kartu su nerūšiuotomis komunalinėmis atliekomis, nes tik taip atsiranda galimybė kuo daugiau atliekų perdirbti, sumažinti aplinkos taršą.

Vartotojų ir atliekų turėtojų šiame procese vaidmuo yra svarbus, nes tik tinkamai rūšiuojant gaminių bei pakuočių atliekas, galima sulaukti teigiamų rezultatų, mažinant aplinkos taršą pakuotės bei gaminių atliekomis.

Elektros ir elektroninės įrangos atliekas mesti į komunalinėms atliekoms skirtus konteinerius, statyti šalia jų, laikyti atviroje vietoje, ardyti patiems, atiduoti
neturintiems leidimų surinkėjams ar metalo supirkimo įmonėms negalima  – tai pavojinga tiek aplinkai, tiek sveikatai.

Nebenaudojamus sugedusius elektronikos prietaisus galite perduoti elektroninių atliekų surinkėjams. Dėl daugiau informacijos apie atliekų surinkimą kreipkitės info@juka.lt