---
title: Modbus Gateways with High Performance and Port Density
date: 2018-11-20 13:54:00 +02:00
categories:
- news
- video
- gateways
tags:
- MB3660
- modbus
- gateways
- high performance
- port density
lang: en
ref: MB3660
layout: page
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/NGZ5s6CvWrY?ecver=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

