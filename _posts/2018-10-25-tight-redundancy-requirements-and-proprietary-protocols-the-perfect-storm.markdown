---
title: Tight Redundancy Requirements and Proprietary Protocols, The Perfect Storm
date: 2018-10-25 11:41:00 +03:00
categories:
- news
- story
tags:
- scada
- moxa
- mxvie
- microsoft
- support
- industrial networking
- protocols
- engineering
lang: en
ref: story
layout: page
---

MOXA interviewed members of Tech Support and Field Engineering Teams to learn about the challenges they encounter in the field and how they have helped customers to overcome their challenges.![tech support.png](/uploads/tech%20support.png)

This story comes from Jean-Marc, a Field Application Engineering Manager. In the story, he tells us about how he worked with an experienced customer to get their redundancy network to meet recovery time requirements while working through proprietary protocol conversions for a new tram system in Africa.

**The Problem**

The customer, an integrated transport systems and rolling stock provider, had recently secured a contract to build a new rail system. They were responsible for signaling, traction substations, telecommunication systems, operational support, maintenance equipment and ticketing systems. To match their new project, the customer was going to need a large and complex network to support these initiatives. It was imperative that this project be completed on time. The customer was actively bidding on projects in other local cities planning to expand their public transportation infrastructure and this project was going to be proof of their timeliness and quality of work.

As an international provider of integrated transport systems and rolling stock, the customer understood the difficulties of deploying in extreme climates. Instead of using enterprise grade products like they normally do, they decided to deliver a full multiservice network utilizing industrial grade products to withstand the heat of the African climate.

Moving to industrial grade products would help to alleviate network failure and free up engineering resources in the future. However, the team was still unfamiliar with industrial device deployment. To remedy this the engineering team received training on how to configure the network access control list (ACL) on the industrial products since this process is reversed when working with enterprise products. They knew this process had to be understood before they could set up the NMS.

Despite the training, designing and deploying the large complex network was proving to be costly in man hours. The customer ran into trouble setting up the network management system (NMS) and connecting it to their SCADA, a Windows Server machine through OPC. This was when they called Jean-Marc.

**The Fix**

Jean-Marc walked the client through configuring the network ACLs. The customer initially had 30 rules possible with the ACL. Keeping these rules straight was confusing, so Jean-Marc worked with the engineers to prioritize and condense the rules. Additionally, there were also 40 VLANs that had been initially deployed and again Jean-Marc worked with the engineers to cut the total number down to 12. Cutting down the number of rules made the system much more manageable and would help to simplify the process of connecting to the NMS.

It was now time to start connecting to the NMS. The SCADA that the engineers used was Matrikon. They installed the SCADA on a windows server similar to that of Moxa’s NMS or MxView. To make them communicate, Jean-Marc worked with the engineers set up an OPC link, a specific interface on Windows for allowing industrial communications. He explained this box is also known as DCOM, a proprietary Microsoft technology for communication between software components of networked computers. The DCOM was then tested for proper configuration on both sides to ensure that they would communicate.

**Result**

Once the DCOM on both sides had been properly configured, the customer was then able to monitor the network through Matrikon, their SCADA system. As an added bonus, with all the facility networks monitored by MxView, the customer liked how they could see a physical topology on their monitors instead of a logical one. Their enterprise software only allowed for logical topologies which can be confusing compared to network diagram visualizations. The physical topology would allow for easier trouble shooting of network issues in the future.

![SCADA.png](/uploads/SCADA.png)

**Summary**

As an integrated transport system and rolling stock provider, it’s essential that the company was able to establish a remote monitoring and maintenance system. Not only is this important for preventing disaster should the line become unusable, but also for preventative maintenance to ensure train schedules are kept. The customer was able to get the SCADA system to integrate with MxView using Microsoft’s proprietary OPC called DCOM. Once this was established, they were able to monitor the network through a diagram of the physical topology.

On a project of this scale, it was essential that the project be completed on time. With other local cities in the area planning to expand their public transportation infrastructure, the train system opening and operating on time is crucial. This initial project will be a testament to the quality of their work and timeliness as the customer will look to win bids for other projects in the near future.

*All information is prepared by MOXA*