---
title: Moxa pristatė "Cloud" pagrįstą saugų nuotolinės prieigos sprendimą, skirtą
  lengvai valdyti nutolusius pramoninius įrenginius
date: 2018-05-09 14:35:00 +03:00
categories:
- news
tags:
- moxa
- cloud
ref: cloud
layout: page
---

![mrc-suite.png](/uploads/mrc-suite.png)

Moxa "Moxa Remote Connect Suite" įranga, leis nesudėtingai ir saugiai valdyti nutolusią įrangą. 

"Suite" yra saugi nuotolinio ryšio valdymo platforma, kurioje yra serverio ir kliento programinė įranga supaprastinantį saugaus ryšio kanalo konfigūravimą ir paleidimą.

Palaikoma "Amazon EC2". 

"Moxa Remote Connect Gateway" saugiai sujungia tinklinius nutolusius įrenginius prie serverio.

„Moxa Remote Connect Client" gali prijungti inžinieriaus nešiojamąjį kompiuterį prie serverio. Šis sprendimas suteikia galimybę automatikos įrangos kūrėjams/valdytojams/prižiūrėtojams lengvai ir saugiai prisijungti prie nuotolinių kompiuterių ir įrangos.  

Daugiau informacijos anglų kalba rasite [čia.](http://juka.lt/moxa-launches-a-cloud-based-secure-remote-access-solution-for-effortless-connection-to-machines/)
