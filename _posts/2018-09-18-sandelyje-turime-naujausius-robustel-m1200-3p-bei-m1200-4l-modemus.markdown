---
title: Sandėlyje turime naujausius Robustel M1200 maršrutizatorius
date: 2018-09-18 14:45:00 +03:00
ref: m1200
layout: page
---

![M1200_Gateway_-_Cellular_Modems_Robustel.png](/uploads/M1200_Gateway_-_Cellular_Modems_Robustel.png)

<iframe width="560" height="315" src="https://www.youtube.com/embed/esvkh3vhRrM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Daugiau informacijos anglų kalba [čia](http://www.robustel.com/products/gorugged-industrial-cellular-m/m1200-gateway.html){:target="_blank"}

Užsisakykite el-paštu info@juka.lt arba 
<form action="http://juka.lt/kontaktai/"> <input type="submit" value="Pateikite prekės užklausimą čia" /> </form>
