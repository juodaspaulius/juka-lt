---
title: Meet us at Industrial fair Instrutec 2018 in Tallinn
date: 2018-11-07 14:41:00 +02:00
tags:
- exhibition
- instrutec
- fair
- industrial networking
lang: en
ref: instrutec2018
layout: page
---

We are participating in Industrial fair Instructec 2018 in Tallinn, Estonia during 8-9th of November.![IMG_7514-2.JPG](/uploads/IMG_7514-2.JPG)

Meet us at C-72 booth. We are looking forward to discuss with you about our official partners Moxa, Robustel C&T and their solutions.

More information about the fair [here.](https://instrutec.ee)

Fair location: Pirita tee 28, Kesklinn, Tallinn, 10127 Harju Maakond, Eesti