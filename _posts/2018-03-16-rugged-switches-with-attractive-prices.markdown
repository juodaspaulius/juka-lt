---
title: Rugged Switches with Attractive Prices
date: 2018-03-16 11:44:00 +02:00
lang: en
ref: eds
layout: page
---

![P1-promo-EDS205_208.jpg](/uploads/P1-promo-EDS205_208.jpg)

When ordered 10 or more **EDS-205** pcs we offer special price - 57€/pcs.

When ordered 10 or more **EDS-208** pcs we offer - 83€/pcs.

To order please email us at info@juka.lt or 
<form action="http://juka.lt/kontaktai/"> <input type="submit" value="Submit request here" /></form>
