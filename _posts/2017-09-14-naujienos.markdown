---
title: Naujas produktas SDS-3008
date: 2017-09-14 15:02:00 +03:00
categories:
- news
- ethernet swithces
tags:
- news
- moxa
- juka lt
- industrial networking
- sds-3008
- naujienos
- ethernet swithces
ref: news1
layout: page
---

![Capture - quick card.PNG](/uploads/Capture%20-%20quick%20card.PNG)

**Moxa** pristato naują pramoninių tinklo komutatorių seriją SDS. Pirmasis produktas – SDS-3008, juos jau turime savo sandėlyje.

Šie išmanieji ypač siauri pramoniniai komutatoriai palaiko:

* Pagrindinius industrinius protokolus EtherNet/IP, PROFINET, and Modbus/TCP

* Žiedinius sprendimus: RSTP/STP

* Valdymo funkcijos Vlan, port mirroring, SNMP

* Diskretinis įėjimas ir rėlinis išėjimas

Daugiau informacijos [čia](https://www.moxa.com/product/SDS-3008.htm){:target="_blank"}

![Capture - quik card 2.PNG](/uploads/Capture%20-%20quik%20card%202.PNG)