---
title: Susitinkame Instrutec 2018 parodoje, Taline
date: 2018-11-07 14:37:00 +02:00
tags:
- exhibition
ref: instrutec2018
layout: page
---

Lapkričio 7-9 dienomis dalyvausime Pramoninėje parodoje Instrutec 2018, Taline, Estijoje.![IMG_7514-2.JPG](/uploads/IMG_7514-2.JPG)

Mus gali sutikti C-72 stende. Mielai pabendrausime apie mūsų oficialius partnerius Moxa, Robustel, C&T bei jų produktus ir paslaugas.

Daugiau informacijos apie parodą [čia.](https://instrutec.ee)

Parodos vieta: Pirita tee 28, Kesklinn, Tallinn, 10127 Harju Maakond, Eesti