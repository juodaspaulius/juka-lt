---
title: MX view - vizualizes your industrial Ethernet network
date: 2017-10-24 12:18:00 +03:00
lang: en
ref: MX
layout: page
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/KKZ83d8zNJ0?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

MX view visualizes your industrial network for live-view management. Watch this short video to see how.