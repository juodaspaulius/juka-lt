---
title: Make the on-going maintenance of your PoE applications easier and less costly
date: 2018-06-22 10:16:00 +03:00
lang: en
ref: PoE
layout: page
---

Did you know that managed PoE switches can eliminate the need to send technicians to reboot remote devices?
<iframe width="560" height="315" src="https://www.youtube.com/embed/94s-sEERt4A" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Moxa presents a variety of maintenance features, which can help make the on-going maintenance of your PoE applications easier and less costly.

**Make sure you have the ability to remotely reboot connected PDs (powered devices)**

If you have experience with a PoE setup in the past, you may have experienced that some devices have the tendency to lock up or malfunction. Oftentimes, the remedy for this issue is a simple reboot of the powered device. However, if your PoE application involves a remote installation site that is difficult to access, this could lead to additional costs and wasted time to have a technician visit the installation site to reboot the device. On top of this, if the powered device happens to be a security camera, then you will have missing security footage until the device can be reset. To avoid this, some PoE switches have the capability to reboot the powered devices from a remote location, saving you the time and cost involved with sending someone to do it manually. Some of Moxa’s PoE switches offer the capability to automatically ping the PDs at regular intervals. If the device doesn’t respond within the allotted time frame, the switch will automatically reboot the device.

**Monitor your connected devices with a Managed Switch**

If your PoE application involves any sensitive or mission critical devices, selecting a managed switch will always be your best option. These switches will generally cost you more up front, but the benefits offered by avoiding unnecessary downtime and lowering your Total Cost of Ownership will make it worth the extra cost in the long run. In the area of PoE, a managed Ethernet switch will provide you with increased visibility and control over the connected devices. Features you should expect from your switch include: Ability to view, monitor, log, and report on all network activity.

* Ability to preserve the network in case of failure, such as redundancy, rate limiting, and storm filtering.
* Ability to set and adjust cyber security settings.
* Ability to set triggered events that notify you in the case of:
   * A PoE port powers On / Off
   * A PoE port experiences overcurrent
   * A PoE port experiences a short circuit
   * System exceeds measured or allocated PoE Budget
   * Switch detects failure in Powered Device
   * Switch detects failure in PoE Function
   * Switch detects low input voltage

**All information is prepared by MOXA*