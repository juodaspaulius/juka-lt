---
title: Moxa Launches A Cloud-based Secure Remote Access Solution for Effortless Connection
  to Machines
date: 2018-05-07 15:44:00 +03:00
categories:
- news
tags:
- moxa
- cloud
- remote
- connection
- machines
lang: en
ref: cloud
layout: page
---

![mrc-suite.png](/uploads/mrc-suite.png)

To serve the needs of OEMs and machine builders who require  moxa-remote-connectremote connections to manage and maintain their machines at remote sites, Moxa has launched the Moxa Remote Connect Suite. The suite is a remote connection management platform that includes server and client software and a hardware gateway to facilitate secure remote access. The Moxa Remote Connect Server is a connection management platform that is hosted on Amazon EC2. The Moxa Remote Connect Gateway connects Ethernet-based edge devices to the server portal securely. Last, the Moxa Remote Connect Client is a software tool that can connect an engineer’s laptop to the server portal. This solution provides OEMs and machine builders with an easy and secure way to connect to their remote machines and equipment to perform troubleshooting, maintenance, data acquisition, and device management remotely, which enables faster and smarter maintenance support.

**Easy Deployment**

The Auto Configuration function makes installation as simple as plug and play and there is no need to configure VPNs or have extensive IT knowledge. In addition, there is no need to perform complex firewall settings or reconfigure IP Addresses. The Moxa Remote Connect provides Smart IP Mapping to avoid IP conflicts and is firewall friendly to comply with existing IT security policies, which allows companies that need secure remote access to minimize the amount of time and effort spent on deployment.

**Enhanced Security**

In order to combat the numerous security risks when data is collected through the Internet, the Moxa Remote Connect is equipped with security features including end-to-end encryption to prevent man-in-the-middle attacks. The Smart Protection function includes an embedded firewall that allows remote access under whitelist control without disrupting local networks. In order to provide an on-demand maintenance service, access to machines is fully controlled by machine operators.
 

**Flexible and Scalable Connectivity**

The Moxa Remote Connect solution allows for greater flexibility by supporting a variety of connections including 1-to-1, multiple-to-multiple, and site-to-site, to satisfy different user scenarios. The Moxa Remote Connect solution also allows companies to remotely manage numerous machines and users at different locations, which makes it easier to manage when businesses expand.


“With its easy to use design, secure connections, and flexible connectivity, the Moxa Remote Connect allows OEMs to access their remote machines effortlessly and streamline their maintenance efforts” stated Li Peng, a product manager in Moxa’s Industrial Ethernet Infrastructure business unit.

**Learn More**

To learn more about the Moxa Remote Connect secure remote access solution, please visit MOXA [microsite](https://www.moxa.com/event/industrial-ethernet/moxa-remote-connect/index.htm){:target="_blank"}, watch the [video](https://www.youtube.com/watch?v=wHGQh5EHr9g&feature=youtu.be){:target="_blank"}, or download the [white paper](https://www.moxa.com/support/request_catalog_detail.aspx?id=3623){:target="_blank"}.

All information is prepared by Moxa.

<iframe width="560" height="315" src="https://www.youtube.com/embed/wHGQh5EHr9g" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

