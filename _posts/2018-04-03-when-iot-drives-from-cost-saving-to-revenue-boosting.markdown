---
title: When IoT Drives from Cost Saving to Revenue Boosting
date: 2018-04-03 15:06:00 +03:00
lang: en
ref: sarun
layout: page
---

Robustel EMEA Director Sarun Kub during Embedded World 2018 exhibition delivered the value added speech "When IoT Drives from Cost Saving to Revenue Boosting".![robustel sarun.jpg](/uploads/robustel%20sarun.jpg)

Watch Full Version of speech [here.](http://events.techcast.com/embedded-world/archiv/EmbeddedWorld_Halle4_Mittwoch_0930/?utm_source=newsletter&utm_medium=email&utm_campaign=remote_access_to_siemens_plc_via_robustvpn_robustel_newsletter&utm_term=2018-04-02)