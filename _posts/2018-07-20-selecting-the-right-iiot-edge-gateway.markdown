---
title: Selecting the Right IIoT Edge Gateway
date: 2018-07-20 12:14:00 +03:00
categories:
- moxa
- gateway
- moxa products
- moxa devices
tags:
- moxa
- industrial networking
- iiot
- edge gateway
lang: en
gateway: 
layout: page
---

![EdgeGateway.png](/uploads/EdgeGateway.png)
The IIoT Edge Gateway is an essential piece to adopting the Industrial Internet of Things. Selecting the right gateway will allow you to collect, connect, and analyze your field data in a quick, safe, and secure way.

If you’re considering an IIoT Edge Gateway for your IIoT architecture, we’ve provided a few benefits to look for and things to consider to find the device that is right for your business.

**Look for proven reliability and proper industry certifications**

As with any market trend, the IIoT will certainly bring a range of new brands and products to the marketplace. Make sure both the partner and product you select has a history of proven successes and understands the variables unique to your industry. When it comes to product selection, extreme temperatures, high levels of electromagnetic interference, and constant vibrations are typical in an industrial environment. Make sure the device you select has the proper industrial certifications and conforms to industrial-grade EMC/EMI/EMS tests.

**Consider your software application needs**

The first area to consider is the device’s OS. IIoT Edge Gateways that come with an open source operating system, such as Debian GNU/Linux, can provide you with more flexibility as you build, modify, and deploy your applications in the future.

Second, you’ll want to consider the data-acquisition software supplied with the gateway. A few things you may want to look for include:

* Support for generic Modbus and MQTT protocols

* Ability to facilitate tag-based data acquisition with virtual-tag support

* Support for RESTful/C/Python APIs for easy deployment

* Built-in client support for your desired cloud service providers

Finally, determine if you’ll want any remote monitoring or communication features with your gateway. Remote communication can reduce system maintenance costs and machine downtime. You want to make sure that your gateway solution comes with built-in remote communication capabilities to address those issues. Having remote access capabilities will also allow you to facilitate the easy transfer of data from your field devices to the cloud by utilizing dashboards and data-visualization tools.

**Make sure the edge gateway is compliant with industrial security standards**

Unauthorized access to critical network infrastructure is a large threat that industrial operators have to deal with today. Because of this, it’s important that your industrial applications are built upon a secure platform. To start, we recommend using an Industrial IoT Edge Gateway that is compliant with IEC 62443, which is the industrial-level security standard.

For the latest in the platform and device security, look for the TPM 2.0 specification. TPM, or Trusted Platform Module, is the international standard for a security cryptoprocessor and is used with software to enable features of integrity measurements, health checks, and authentication services. With TPM 2.0, IIoT Edge Gateways provide tailored levels of security depending on the application needs. This means high-security features are available for industrial and mission-critical applications.

**Look for a partner who has your back**

Whether it’s a long-term warranty, built-in protocol support, or general tech support, it helps to have a vendor you can count on for long-term success. At Moxa, we provide a global support network and over 30 years of experience reliably connecting industrial machines and devices. Additionally, our commitment to extended product lifecycles will ensure your operations will be supported for years to come.

*All information is prepared by MOXA*