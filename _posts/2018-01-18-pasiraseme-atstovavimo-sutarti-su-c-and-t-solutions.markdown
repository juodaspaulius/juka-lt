---
title: Pasirašėme atstovavimo sutartį su C&T Solutions
date: 2018-01-18 15:59:00 +02:00
ref: cnt
layout: page
---

![VIO_Family_1-5c6258.png](/uploads/VIO_Family_1-5c6258.png)

**Juka LT** pasirašė atstovavimo sutartį su pramoninių kompiuterių gamintoju **C&T Solutions inc.**, kuris siūlo sprendimus skirtus pramonės automatizavimui, transporto ir geležinkelių sektoriams. 

**C&T Solutions** gamina pramoninius kompiuterius, pramoninius monitorius ir įvairias kompiuterių plokštes, skirtus sudėtingoms aplinkos sąlygoms.

[Daugiau apie C&T Solution ](http://www.candtsolution.com/en/){:target="_blank"}

<form action="http://juka.lt/kontaktai/"> <input type="submit" value="Pateik prekės užklausimą" /> </form>

![C&T_Logo_UnderlineText.jpg](/uploads/C&T_Logo_UnderlineText.jpg)![BCO-1030_Rear-dbc2f2.png](/uploads/BCO-1030_Rear-dbc2f2.png)![BCO-1030_Front-d52ec3.png](/uploads/BCO-1030_Front-d52ec3.png)![CT-MSL01-9dd366.png](/uploads/CT-MSL01-9dd366.png)