---
title: Robustel MEG5000 Laimėjo Red Dot Product Design Awards 2018!
date: 2018-04-11 13:16:00 +03:00
ref: meg5000
layout: page
---

[![robustel web.jpg](/uploads/robustel%20web.jpg)](http://www.robustel.com/products/modular-Edge-Gateway/meg5000-modular-edge-gateway.html){:target="_blank"}

Daugiau informacijos apie apdovanojimą anglų kalba rasi [čia](http://juka.lt/robustel-meg5000-wins-red-dot-product-design-award-2018/)

[Sužinok daugiau apie MEG5000](http://www.robustel.com/products/modular-Edge-Gateway/meg5000-modular-edge-gateway.html){:target="_blank"}