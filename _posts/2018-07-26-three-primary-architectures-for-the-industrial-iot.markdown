---
title: Emerging IIoT Architectures
date: 2018-07-26 09:42:00 +03:00
categories:
- moxa
- news
tags:
- moxa
- iot
- IIOT
lang: en
ref: architectures
layout: page
---

[![Three_Primary_Architectures_for_The_Industrial_IoT.png](/uploads/Three_Primary_Architectures_for_The_Industrial_IoT.png)](http://pages.moxa.com/Moxa-Industrial-Internet-Of-Things-Architectures.html?utm_source=post&utm_medium=facebook&utm_campaign=gbl-18-iiot&utm_content=1529358015){:target="_blank"}

When implementing various IoT technologies into your business, data acquired from your operations is generally transmitted and stored on third party hosted cloud servers. This approach is very different from the traditional enterprise IT approach, where information is saved on local servers. By leveraging third party cloud services, OT device data and overall system health can be remotely accessed, monitored, and managed by connecting to the cloud server through the Internet. However, as more businesses embark to adopt Industrial IoT technology, they quickly begin to realize that there are many different approaches to connecting their edge data.

In this article, we highlight three primary IIoT architectures we see emerging with our customers, as well as how they work and the pros / cons we’ve identified for each.

**Edge to Cloud**

![Edge-to-Cloud-Diagram_vertical.png](/uploads/Edge-to-Cloud-Diagram_vertical.png)

Edge to cloud is a common architecture that utilizes IIoT Edge Gateways to act as a middleman between edge devices and the Cloud. The IIoT Edge Gateway performs several important tasks, including translating industrial protocols, processing data locally, and filtering and transmitting selected data to the Cloud (which can be public or private).

Several industries are adopting this architecture, as it is similar to the traditional M2M or remote monitoring applications. However, with this approach, customers are adding IIoT Edge Gateways to the existing infrastructure and relying on Cloud infrastructure to provide device management, storage and dashboards for visualization. Many also plan or are already utilizing the data analytics and even some machine learning capabilities that these platforms offer. These features are great for gathering insights to your connected devices and operations performance, but also come at an added cost. The cost will typically vary depending on the amount of data that needs to be processed and where your operations are located.

**Pros:**

* Highly scalable

* Centralized storage and data visualization

* Powerful analytics through Cloud platforms

**Cons:**

* Quality of data accuracy (large applications with Big Data processing)

* Potential latency, affecting real-time data

* High bandwidth costs as the application grows

* Additional costs to access advanced analytics modules from the Cloud platform

**Edge to Modern Web SCADA**

![Edge-to-Modern-SCADA_vertical.png](/uploads/Edge-to-Modern-SCADA_vertical.png)

The next popular architecture is a new trend of hosting SCADA solutions on the cloud instead of local computers, often using a Software as a Service (SaaS) business model. These online Web SCADA systems would likely be hosted on a popular cloud service such as Azure, AWS, or Google Cloud Platform, speak today’s Restful Web APIs, and often use the same MQTT or AMQP protocols we see in Edge to Cloud architectures.

Although most industrial customers are still using the traditional model of in-house SCADA systems, we are witnessing sections of operations switching to Web SCADA systems so they can take advantage of the benefits provided by this new architecture. To help you understand whether the “Edge to Modern SCADA” model is right for your operations, here are some pros and cons to consider.

**Pros:**

* Significantly lower implementation and maintenance costs compared to physical SCADA systems

* Very fast deployment

* Easy access to real-time and historical information for multiple parties

* 3rd party technical support

* Scalable server space with a pay-as-needed model

* Automatic updates to SCADA software

**Cons:**

* Increased cybersecurity threats

* Bandwidth limited to your ISP service

* Dependent on the reliability of your cloud service (Azure, AWS, etc.)

**Edge to Traditional SCADA**

![Edge-to-Traditional-SCADA_vertical.png](/uploads/Edge-to-Traditional-SCADA_vertical.png)

The third common architecture we are seeing is Edge to Traditional SCADA. One important area for this type of architecture are the APIs. Traditional SCADA systems must have their Polling protocols converted to new RestFul web APIs, or even better to Publish/Subscribe protocols with Statefulness such as MQTT.

In this third architecture, we are seeing large industrial providers including GE, Schneider and Siemens offering their own Cloud software solutions.  Many of these are hosted on AWS or Azure, but those details are often abstracted from the user. In other cases, we are seeing some of these customers connect directly to Azure or AWS.

**Pros:**

* Full control of SCADA system and network

* Increased security, especially from cyber attacks

* Increased reliability and system availability

**Cons:**

* Significant upfront investment

* Maintenance is tedious, time-consuming, and requires access to SCADA programmers

* More difficult and costly to expand upon

It’s important to note that the 3 architectures shown above can be much more complex in reality. In many cases, customers might use two or even three of these architectures in deployments, or they have various combinations of Cloud and SCADA solutions.

To enable any of these architectures, you need IIoT Edge Gateways that can handle selected data acquisition locally, while connecting your edge devices to cloud or web-based systems. To learn more about IIoT Edge Gateways, continue on to our next article.

*All information is prepared by [Moxa](http://pages.moxa.com/Moxa-Industrial-Internet-Of-Things-Architectures.html?utm_source=post&utm_medium=facebook&utm_campaign=gbl-18-iiot&utm_content=1529358015){:target="_blank"}*