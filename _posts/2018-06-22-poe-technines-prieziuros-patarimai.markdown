---
title: PoE techninės priežiūros patarimai
date: 2018-06-22 10:31:00 +03:00
ref: PoE
layout: page
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/94s-sEERt4A" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Daugiau informacijos anglų kalba [čia](http://juka.lt/make-the-on-going-maintenance-of-your-poe-applications-easier-and-less-costly/)