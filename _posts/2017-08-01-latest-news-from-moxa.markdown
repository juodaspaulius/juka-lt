---
title: Latest news from MOXA
date: 2017-08-01 15:54:00 +03:00
categories:
- moxa
- news
tags:
- moxa
- news
lang: en
layout: page
---

MOXA solutions to protect industrial networks from edge to cloud.
![edge to cloud.jpg](/uploads/edge%20to%20cloud.jpg){:target="_blank"}

[More](https://www.moxa.com/Event/integrated-solutions/security/edge-to-cloud/index.htm?utm_source=2017_07_Spotlight&utm_medium=email&utm_campaign=Spotlight&utm_content=Spotlight_Banner1&mkt_tok=eyJpIjoiTkRsa05EYzVaRE14WTJVNSIsInQiOiJxMVlmcXBKMnNkOFpnbUxHTVNGNWk1cjlTVTBKcUVMRGU3U2d4WGpIazRqNjd6UXpmOFRWWm9pUm0xTnoxV1VvNmYrbFNLdXNLbWx4N2x6eFgwNVdZKzQyRUZYK3BmR2F0cjlMd01NSGorRkRPVXpXV094RXVpalNHNVExMEVZbCJ9){:target="_blank"}