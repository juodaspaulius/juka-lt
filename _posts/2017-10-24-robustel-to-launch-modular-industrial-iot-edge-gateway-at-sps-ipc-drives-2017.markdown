---
title: Robustel to Launch Modular Industrial IoT Edge Gateway at SPS IPC Drives 2017
date: 2017-10-24 10:12:00 +03:00
lang: en
ref: robustel news
layout: page
---

![Robustel_to_Launch_Modular_Industrial_IoT_Edge_Gateway](/uploads/Robustel_to_Launch_Modular_Industrial_IoT_Edge_Gateway_at_SPS_IPC_Drives_2017_-_Press_Release_-_Robustel_Industrial_Wireless_M2M_-_2017-10-24_10.18.02.png)

Robustel, a leading industrial IoT/M2M hardware and solution provider is about to launch its new modular industrial IoT edge gateway MEG5000 at SPS IPC Drives in Nuremberg, Germany, November 28 to 30. MEG5000 supports various communication protocols to facilitate fast application development.

MEG5000 is flexible, easy to configure and manage. It features three scalable cards that support a variety of interfaces to meet the changing demands of applications in the Industrial Internet of Thing (IIoT). Thanks to quick deployment and simple customization the gateway can be tailored to virtually any industrial requirement.

Through edge computing, the MEG5000 gateway is able to process data right at the network edge and in real time. It can therefore collect, analyze and act on data more efficiently, and supports data optimization which is vital in the IIoT. When transferring large amounts of data to the cloud over limited bandwidth, latency might occur. MEG5000 supports up to 1300 Mbps Wi-Fi speed ensuring that its edge computing power helps to reduce data uploads by acting like a local, edge-based cloud service tool.

The powerful gateway is based on RobustOS, a Linux-based operating system that was developed by Robustel specifically for the use in Robustel devices. RobustOS includes basic networking features and protocols, providing customers with proficient user experience. In addition, Robustel offers partners and customers Software Development Kit (SDK) that allows further customization by using C, Python or Java. It also provides a rich amount of Apps to meet the diverse IoT market requirements.

Additional MEG5000 features:

*  1 x 10/100/1000 Mbps WAN and 8 x 10/100 Mbps LAN
*  802.11a/b/g/n/ac, supporting AP and Client modes
*  High performance, reliability and throughput for optimized data processing
*  Embedded mSATA SSD for data logging
*  Embedded digital temperature sensor for detecting internal running temperature
*  IPsec/ OpenVPN/ GRE/ L2TP/ PPTP/ DMVPN
*  Optional Backup Mode supporting cold, warm and load balancing
*  Comprehensive security and encryption features
*  Notification via SMS, e-mail, DI/DO, SNMP trap or RobustLink
*  Robust industrial design with 9 to 60 VDC power input and desktop, wall, or DIN rail mounting

[Find more about Robustel](http://www.robustel.com/){:target="_blank"}