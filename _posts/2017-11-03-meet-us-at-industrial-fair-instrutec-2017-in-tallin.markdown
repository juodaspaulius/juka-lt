---
title: Meet us at Industrial fair Instrutec 2017 in Tallin
date: 2017-11-03 11:52:00 +02:00
categories:
- fair
- exhibition
- industrial networking
tags:
- fair
- exhibition
- news
lang: en
ref: instrutec
layout: page
---

We are participating in Industrial fair Instructec 2017 in Tallin, Estonia during 15-17th of November.

![instrutec-2017.jpg](/uploads/instrutec-2017.jpg)

Meet us at **C-42 booth**. We are looking forward to discuss with you about our official partners Moxa and Robustel and their solutions.

More information about the fair [here. ](https://instrutec.ee/en/){:target="_blank"}

Fair location: Pirita tee 28, Kesklinn, Tallinn, 10127 Harju Maakond, Eesti