---
title: Robustel MEG5000 Wins Red Dot Product Design Award 2018!
date: 2018-04-11 13:09:00 +03:00
lang: en
ref: meg5000
layout: page
---

[![robustel web.jpg](/uploads/robustel%20web.jpg)](http://www.robustel.com/products/modular-Edge-Gateway/meg5000-modular-edge-gateway.html){:target="_blank"}

Munich, Germany, April 11, 2018 – Robustel is pleased to announce that its Modular Edge Computing Gateway **MEG5000** has received the **Red Dot Product Design Award** for 2018 in the Communication Technology category.

The [Red Dot Design Award](https://en.red-dot.org/){:target="_blank"} is one of the world’s most renowned and respected awards for product design. In 2018, designers and manufacturers from 59 countries submitted more than 6,300 objects to the competition. The strict judging criteria, which include level of innovation, functionality, formal quality, ergonomics and durability, provide a frame of reference which the jurors then complement with their own expertise.

**MEG5000 Modular Edge Computing Gateway** for Internet of Things (IoT) features high speed data processing capability. It features three scalable cards, supporting various interfaces on customization to meet changing demands for industrial IoT applications, including sectors like Industrial Automation, Industry 4.0 and Industrial Communication.

[Find more about MEG5000](http://www.robustel.com/products/modular-Edge-Gateway/meg5000-modular-edge-gateway.html){:target="_blank"}