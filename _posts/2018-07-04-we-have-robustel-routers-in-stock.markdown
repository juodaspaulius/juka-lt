---
title: We have Robustel Routers in stock
date: 2018-07-04 12:32:00 +03:00
lang: en
ref: R2000-E4L2
layout: page
---

[![R2000 Ent -1.jpg](/uploads/R2000%20Ent%20-1.jpg)]((http://www.robustel.com/products/gorugged-industrial-cellular-r/r2000-ent-router.html)){:target="_blank"}

We have in stock Robustel R2000-E4L2 Routers for 330 EUR only. 

Robustel R2000 Ent Dual Module Cellular Router with Voice provides fast and reliable Internet connectivity, enhanced voice capabilities – making it perfect to respond to and manage any device, anytime and anywhere. 

More information about Router [here.](http://www.robustel.com/products/gorugged-industrial-cellular-r/r2000-ent-router.html){:target="_blank"}

Please contact us at juka@info.lt in order to place an order.
