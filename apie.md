---
title: Apie
position: 14
ref: about
layout: default
---

**JUKA LT, UAB** yra pramoninio tinklo įrangos bei sprendimų tiekėjai.

Esame **MOXA**, **ROBUSTEL** ir **BitStream** atstovai Baltijos šalyse. Daugiau apie partnerius galite sužinoti [čia](http://juka.lt/partneriai/){:target="_blank"}.

Mūsų vizija yra tapti lyderiaujančiais įrangos platintojais suteikiant pridėtinę vertę tiek sistemų integratoriams tiek galutiniams vartotojams.

**JUKA LT** komandos teikiamos paslaugos:

* Pramoninių duomenų perdavimo sprendimų projektavimas ir diegimas
* Programinės įrangos kūrimas ir pritaikymas pagal kliento poreikius
* Atliekame pramoninių tinklų auditą, topologinius brėžinius ir t.t.
* Nestandartinių produktų tiekimas