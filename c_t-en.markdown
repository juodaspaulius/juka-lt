---
title: C&T Solution
date: 2018-08-09 06:46:00 +03:00
position: 4
lang: en
ref: c_t
parent: partners
layout: page
---

![C&T_Logo_UnderlineText.jpg](/uploads/C&T_Logo_UnderlineText.jpg){:target="_blank"}

C&T Solution Inc. is JUKA LT partner that focuses on providing industrial/embedded computing solutions for industrial automation, medical computing, transportation, networking and other industries, C&T strives for the highest standards in innovation and technology, enabling the company to supply a wide ranging portfolio consisting of embedded computer systems, display systems, and industrial computer boards that are able to withstand extremes in environmental conditions, shock and vibration.

[Find out more about C&T Solution ](http://www.candtsolution.com/en/){:target="_blank"}

<form action="http://juka.lt/en/contacts/"> <input type="submit" value="Submit quotation request" /> </form>
